pip install unidecode

pip install difflib

pip install re

pip install faker

pip install gender_guesser

pip install random

#########################################################################################

import unidecode
"""
    remplace les caractères accentué par leur equivalent non accentué
"""
def accent(text):
    return unidecode.unidecode(text)

#########################################################################################

import difflib

def identification(pattern, tab_text, ratio=0.75):
    """
        Fonction qui revoir un boolen selon le ration de similarité entre deux chaine de caractère
    """
    test = difflib.get_close_matches(pattern, tab_text ,cutoff=ratio ) # on recherche 80% de similarité 
    est_iden = not test
    return not est_iden 

#########################################################################################

def terme_trouvé(Termes, Text):
    """
    rechercher une liste de termes donnés en entrer comparer à une liste de mots
    """
    trouve = False
    i = 0
    while not trouve :
        if i >(len(Text)-4):
            break
        fenetre = Text[i:i+4]
        for terme in Termes:
            cpt=0
            for mot in terme:
                if identification(mot , fenetre):
                    cpt +=1
                    if cpt > len(terme)/2:
                        trouve = True
                        #print('fenetre:',fenetre)
                        return trouve
        i+=1
    return trouve

#########################################################################################

import re

def pretraitement(text):
    """
        Fonction qui prétraite l' entree text et retourne si un des terme est present dans l'entree
    """
    text = text.lower() # mise en minuscule des caractères
    text = accent(text) # décode les accents du text
    Text = re.split(r"[\W']+", text)
    Termes = [['horton'],["arterite","cellule","geante"]]
    return terme_trouvé(Termes,Text)


#########################################################################################
# Generer Nouveaux Nom
#########################################################################################

import gender_guesser.detector as gender

def detectGender(surname):
    """
    fonction utilisant l'implémentation de la librairy gender afin de détecter le genre d'un prenom
    sortie : ["male","female","mostly_female","mostly_male","unknown"]
    """
    d = gender.Detector()
    genre = d.get_gender(surname)
    typeGenre = -1
    if genre in ["female","mostly_female"]:
        typeGenre = 0
    if genre in ["male","mostly_male"]:
        typeGenre = 1
    return typeGenre  

#########################################################################################

from faker import Faker
from faker import providers

from faker import Faker
fake = Faker(['fr_FR'])

def generateFirstName(typeGenre):
    """
    Fonction qui revoie un prenom selon le genre signifié en entrée avec la library python faker
    typeGenre 0:female 1:Male 
    """
    if typeGenre==0 :
        return fake.first_name_female()
    elif typeGenre==1:
        return fake.first_name_male()
    else:
        return fake.first_name_nonbinary()

def generateLasttName():    
    """
    Fonction qui revoie un nom avec la library python faker
    """
    return fake.last_name()

#########################################################################################

def short_name( NB_lettre, typeGenre, typeName):
    # typeGenre: female:0 Male:1 - typeName: LasttName(N):0 FirstName(PN):1
    try:
        Short_N = {(0,0,1): [''],            
                   (0,1,1): [''],
                   (0,-1,1): [''],
                    (0,-1,0): [''],
                   (1,1,1): ['X'],
                   (1,0,1): ['X'],
                   (1,-1,1): ['X'],
                   (1,-1,0): ['A','O','T','X','Z'],
                   (2,1,1): ['Zi','So','Lu','Le','Ma'],
                   (2,0,1): ['Zi','So','Lu','Le','Ma'],
                   (2,-1,1): ['Zi','So','Lu','Le','Ma'],
                   (2,-1,0): ['Li','Ta','Ma','Zu'],
                   (3,1,1): ['Léa','Mia','Zoé','Eve','Ava','Eva','Émy'],
                   (3,0,1): ['Maé','Noa','Sky','Ari','Noe','Sam','Luc'],
                   (3,-1,1): ['Maé','Sky','Ari','Noe','Sam'],
                   (3,-1,0): ['Tao','Zef','Chen','Air','Ten' ,'Tan']
                   }
        name = random.choice(Short_N[(NB_lettre, typeGenre, typeName)])
        return name
    except:
        return ''

#########################################################################################

def sameLonger(oldName , typeGenre, typeName):
    """
    Fonction qui appelle les fonction de génération de noms et retoun un nouveau nom de meme taille
    typeName :LasttName(N):0 FirstName(PN):1
    """
    newName=''
    if len(oldName)<4:
        newName = short_name(oldName , typeGenre, typeName)
    else:
        while len(oldName)!= len(newName) and oldName !=newName :
            if typeName==0:
                newName = generateLasttName()
            if typeName==1:
                newName = generateFirstName(typeGenre)
    return newName

#########################################################################################

def change_name(oldName , typeGenre, typeName):
    """
    Fonction qui appelle les fonction de génération de noms et retoun un nouveau nom de meme taille
    typeGenre: female:0 Male:1 - typeName: LasttName(N):0 FirstName(PN):1
    """
    T = len(oldName)
    newName = []
    if T < 10:
       newName.append(sameLonger(oldName , typeGenre, typeName))
    else:
        while T //10 > 1:
            name = sameLonger('123456789', typeGenre, typeName)
            newName.append(name)
            T-=10
        if T<14:
            Seq = random.randint(4,T-5)   
        else:
            Seq = random.randint(T-10,9)

        name = sameLonger(oldName[:Seq], typeGenre, typeName)
        newName.append(name)
        name = sameLonger(oldName[:abs(Seq-T)-1], typeGenre, typeName)
        newName.append(name) 
    newName = '-'.join(newName)
    return newName

#########################################################################################

def generateNewName(firstName,lastName): # prenom, nom
    """
        fonction qui genere de nouveau nom 
        typeGenre: female:0 Male:1 - typeName: LasttName(N):0 FirstName(PN):1
    """
    typeGenre = detectGender(firstName) # On recupere le genre supposé du prenom 7
    NewFirstName = change_name(firstName , typeGenre, 1)
    NewLastName = change_name(lastName , 0, 0)

    return NewFirstName,NewLastName

#########################################################################################
Anonymisation
#########################################################################################

#Génération de RegEx

#########################################################################################

def regEx_BegEnd(word):
    """"Fonction qui renvoir la regex de l'expression commençant par la premiere lettre et finissant par la dernierère"""
    T = len(word)
    #reg = 'r"\\b'+word[0]+'\w*?'+word[-1]+'"'
    reg = '\\b['+word[0].lower()+','+word[0].upper()+'][\w]{'+str(T-3)+','+str(T-1)+'}['+word[-1].lower()+','+word[-1].upper()+']' #'\w*?' // reg = '\\b'+word[0]+'[\w]{'+str(T-3)+','+str(T-1)+'}'+word[-1] #'\w*?'
    return reg

#########################################################################################

def identifierOccurences(reg,text):
    """
        Fonction qui retourne les occurence identifiees dans un text selon un e RegEx
    """
    Occurrences = []
    for match in re.finditer(reg, text):
        Occurrences.append(match.span())
    return Occurrences
#########################################################################################

def searchAndReplace(oldName,newName,text):
    """
    Fonction qui identifie les occurence d un nim dans un text et qui remplace chaque occurence qui lui sont a 70% siilaire par un autre nom
    """
    reg = regEx_BegEnd(oldName) # Creation de la regEx pour identifier le nom
    Occus = identifierOccurences(reg,text) # création de la liste contenant les position des occurences trouvées de ce nom 
    textA = text
    for occu in Occus:
        terme  = textA[occu[0]:occu[1]]
        #print(terme)
        if identification(oldName, [terme]): # manque le ratio
            #print('              identifier')
            epsilon= abs(len(terme)-len(oldName))
            if len(terme) > len(oldName):
                inser = newName+' '*epsilon
            elif len(terme) < len(oldName):
                inser = newName[0:-epsilon]
            else:
                inser = newName
            textA="".join((textA[:occu[0]],inser,textA[occu[1]:]))
    return textA

#########################################################################################
Processus d'anonymisation
#########################################################################################

def anonymisation(firstName,lastName,text):
    """
        fonction qui genere de nouveau nom, identifie les occurences des autres nom dans un text et les subtitues au nouveau nom
    """
    if pretraitement(text): # Si l'on trouve un terme relatif à la maladie d horton
        newFirstName , newLastName = generateNewName(firstName,lastName)
        #print('prenom:',firstName,'new:',newFirstName)
        text = searchAndReplace(firstName,newFirstName,text)
        #print('nom:',lastName,'new:',newLastName)
        text = searchAndReplace(lastName,newLastName,text)
    return text

#########################################################################################